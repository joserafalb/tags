<?php
/**
 * Created by PhpStorm.
 * User: jose.lopez
 * Date: 10/12/2018
 * Time: 7:51 AM
 */

include('ApiHelper.php');

$file_name = 'items.xml';

$api = new ApiHelper();

$xml = simplexml_load_file($file_name);
foreach($xml->children() as $item)
{
    $item_id = $item->itemID;
    if ($item_id < 596) {
        continue;
    }

    // Check if weight is assigned
    if (isset($item->ItemECommerce->weight) && $item->ItemECommerce->weight > 0) {
        continue;
    }

    // If we got here, it means that we need to update this item.
    $xml_update_parameteres = '<Item><ItemECommerce><weight>10</weight></ItemECommerce></Item>';
    $api->updateItemTag($item_id, $xml_update_parameteres );
    sleep(5);
    file_put_contents('update_logs.txt', 'Item updated: ' . $item_id . "\r\n", FILE_APPEND);
}
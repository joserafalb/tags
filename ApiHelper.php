<?php
/**
 * Created by PhpStorm.
 * User: jose.lopez
 * Date: 10/10/2018
 * Time: 9:43 AM
 */

class ApiHelper
{
    const CLIENT_ID = '3c7521b7d515b82b0ee62bda99accf2059734a44d023913883d3b522a42db140';
    const CLIENT_SECRET = 'bf733a8bdf1d9e9c9f848c8bd896ffe157dbd7e8e9287f83f3cdc79f1e90d185';
    const REFRESH_TOKEN = 'e8958c25f4e17801d2964b377bd2069f2a3bef77';
    private static $api_token;
    private static $expiration_time;

    /**
     * @return mixed
     */
    public static function getApiToken()
    {
        return self::$api_token;
    }

    /**
     * @param mixed $api_token
     */
    public static function setApiToken($api_token)
    {
        self::$api_token = $api_token;
    }

    /**
     * @return mixed
     */
    public static function getExpirationTime()
    {
        return self::$expiration_time;
    }

    /**
     * @param mixed $expiration_time
     */
    public static function setExpirationTime($expiration_time)
    {
        self::$expiration_time = $expiration_time;
    }

    public function isTokenValid()
    {
        if (self::getExpirationTime() != null) {
            $now = new DateTime();
            if (self::getExpirationTime() < $now) {
                $this->refreshToken();
            }
        } else {
            $this->refreshToken();
        }
    }

    public function refreshToken()
    {
        $url = 'https://cloud.lightspeedapp.com/oauth/access_token.php';

        $postfields = array(
            'refresh_token' => self::REFRESH_TOKEN,
            'client_secret' => self::CLIENT_SECRET,
            'client_id' => self::CLIENT_ID,
            'grant_type' => 'refresh_token'
        );

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, sizeof($postfields));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo 'cURL Error #:' . $err;
            die();
        } else {
            // Refresh variables
            $json = json_decode($response);
            $this->setExpirationTime((new DateTime())->add(new DateInterval('PT' . $json->expires_in . 'S')));
            $this->setApiToken($json->access_token);
            echo 'Token Refreshed' . "\r\n";
            sleep(2);
        }
    }

    public function getAllItems($offset = '0')
    {
        $this->isTokenValid();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.lightspeedapp.com/API/Account/163754/Item?offset=" . $offset . '&load_relations=%5B%22ItemECommerce%22%5D');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $headers = array();
        $headers[] = "Authorization: Bearer " . self::getApiToken();
        $headers[] = "Cache-Control: no-cache";
        $headers[] = "Postman-Token: 0d2dca02-d49b-52fa-2438-d720f70bd711";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            die('getAllItems - Error:' . curl_error($ch));
        }
        echo 'All Items retrieved'. "\r\n";
        curl_close($ch);
        return $result;
    }

    public function updateItemTag($item_id, $xml_to_post)
    {
        $this->isTokenValid();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.lightspeedapp.com/API/Account/163754/Item/" . $item_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_to_post);

        // this function is called by curl for each header received
        curl_setopt($ch, CURLOPT_HEADERFUNCTION,
            function($curl, $header) use (&$headers)
            {
                $len = strlen($header);
                $header = explode(':', $header, 2);
                if (count($header) < 2) // ignore invalid headers
                    return $len;

                $name = strtolower(trim($header[0]));
                if (!array_key_exists($name, $headers))
                    $headers[$name] = [trim($header[1])];
                else
                    $headers[$name][] = trim($header[1]);

                return $len;
            }
        );

        $headers = array();
        $headers[] = "Authorization: Bearer " . self::getApiToken();
        $headers[] = "Cache-Control: no-cache";
        $headers[] = "Postman-Token: 0d2dca02-d49b-52fa-2438-d720f70bd711";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = '';
        for ($i = 0; $i <= 3; $i++) {
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                if ($i > 3) {
                    die('updateMatrixTag - Error:' . curl_error($ch));
                } else {
                    $i++;
                }
                sleep(10);
            } else {
                break;
            }
        }

        if (strpos($result, 'Error') !== false) {
            die('Error response. Item Id: ' . $item_id . '. Response: ' . $result);
        }

        $bucket_info = explode('/', $headers["x-ls-api-bucket-level"][0]);
        echo 'Item updated: ' . $item_id .'. Bucket stat: ' . $headers["x-ls-api-bucket-level"][0] .
            '. Bucket limit: ' . $bucket_info[1] . "\r\n";
        if ($headers["x-ls-api-bucket-level"][0] > $bucket_info[1] - 10) {
            echo 'Filling bucket' . "\r\n";
            sleep(10);
        }
        curl_close($ch);
        return $result;
    }

    public function updateMatrixTag($item_id, $xml_to_post)
    {
        $this->isTokenValid();
        $ch = curl_init();
        $headers = [];
        curl_setopt($ch, CURLOPT_URL, "https://api.lightspeedapp.com/API/Account/163754/ItemMatrix/" . $item_id);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_to_post);

        // this function is called by curl for each header received
        curl_setopt($ch, CURLOPT_HEADERFUNCTION,
          function($curl, $header) use (&$headers)
          {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) // ignore invalid headers
              return $len;

            $name = strtolower(trim($header[0]));
            if (!array_key_exists($name, $headers))
              $headers[$name] = [trim($header[1])];
            else
              $headers[$name][] = trim($header[1]);

            return $len;
          }
        );
        

        $headers = array();
        $headers[] = "Authorization: Bearer " . self::getApiToken();
        $headers[] = "Cache-Control: no-cache";
        $headers[] = "Postman-Token: 0d2dca02-d49b-52fa-2438-d720f70bd711";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = '';
        for ($i = 0; $i <= 3; $i++) {
            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                if ($i > 3) {
                    die('updateMatrixTag - Error:' . curl_error($ch));
                } else {
                    $i++;                    
                }
                sleep(10);
            } else {
                break;
            }
        }       

        if (strpos($result, 'Error') !== false) {
            die('Error response. Item Id: ' . $item_id . '. Response: ' . $result);
        }

        $bucket_info = explode('/', $headers["x-ls-api-bucket-level"][0]);
        echo 'Item updated: ' . $item_id .'. Bucket stat: ' . $headers["x-ls-api-bucket-level"][0] .
            '. Bucket limit: ' . $bucket_info[1] . "\r\n";
        if ($headers["x-ls-api-bucket-level"][0] > ($bucket_info[1] - 10)) {
            echo 'Filling bucket' . "\r\n";
            sleep(10);
        }
        curl_close($ch);
        return $result;
    }
}
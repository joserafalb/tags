<?php
/**
 * Created by PhpStorm.
 * User: jose.lopez
 * Date: 10/12/2018
 * Time: 7:51 AM
 */

include('ApiHelper.php');

$file_name = 'tags_to_update.csv';

$api = new ApiHelper();

$row = 1;
if (($handle = fopen($file_name, "r")) !== FALSE) {
    while (($line = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
        if (count($line) != 2) {
            continue;
        }
        $sl_id = $line[0];
        $tags = $line[1];

        echo 'Procesing Item Id: ' . $sl_id . "\r\n";

        // Delete duplicated tags
        $ary_tags = explode(',', $tags);

        // Loop tags and remove duplicated
        $ary_distinct_tags = [];
        foreach ($ary_tags as $tag) {
            $tag = trim($tag);

            // skip empty tags
            if ($tag == '' || $tag == 'hidden') {
                continue;
            }

            $tag = strtolower($tag);

            // Rename tag to use proper tags
            $tag = fixTagName($tag);

            // Check if tag is not duplicated before adding it to the list
            if (in_array($tag, $ary_distinct_tags) === false) {
                $ary_distinct_tags[] = $tag;
            }
        }

        // Send update request to API for this Item
        $xml_to_post = buildXML($ary_distinct_tags);
        $api->updateItemTag($sl_id, $xml_to_post);
        sleep(10);
    }
    fclose($handle);
}

function buildXML($ary_tags)
{
    $tag_count = count($ary_tags);
    $result = '<Item><Tags count="' . $tag_count . '">';

    foreach ($ary_tags as $tag) {
        $result .= '<tag>' . $tag . '</tag>';
    }
    $result .= '</Tags></Item>';
    return $result;
}

function fixTagName($tag)
{
    $result = $tag;
    $ary_tags_to_rename = [
        'Sets' => 'set',
        'SETS' => 'set',
        'Tops' => 'top',
        'TOPS' => 'top',
        'SKIRT' => 'skirt',
        'Skirts' => 'skirt',
        'dresses' => 'dress',
        'DRESSES' => 'dress',
        'DRESS' => 'dress',
        'Dress' => 'dress',
    ];
    if (array_key_exists($tag, $ary_tags_to_rename)) {
        $result = $ary_tags_to_rename[$tag];
    }
    return $result;
}
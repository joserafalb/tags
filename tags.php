<?php

include('ApiHelper.php');


$api = new ApiHelper();
$xml_items = $api->getAllItems(0);
$main_xml = simplexml_load_string($xml_items) or die("Error: Cannot create object from Items XML");

// Get how many items
$items_count = $main_xml->attributes()['count'];

// Calculate how many request we must do. We can pull up to 100 items by request
$request_count = ceil($items_count / 100);

for ($i = 1; $i < $request_count; $i++) {
    // Make request to API
    $xml = simplexml_load_string($api->getAllItems($i * 100));

    echo('Request ' . $i . ' of ' . $request_count . "\r\n");

    // Loop Items
    foreach ($xml->children() as $item) {
        sxml_append($main_xml, $item);
    }

    sleep(3);
}

$main_xml->asXML('items.xml');

// Open csv file

// Loop file

// If SKU is empty, continue to the next line

// Get tag and sku

// Search sku on ls and get ID

// Update Item tag

function sxml_append(SimpleXMLElement $to, SimpleXMLElement $from) {
    $toDom = dom_import_simplexml($to);
    $fromDom = dom_import_simplexml($from);
    $toDom->appendChild($toDom->ownerDocument->importNode($fromDom, true));
}
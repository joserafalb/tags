<?php
class LightSpeed 
{
	const ITEMS_FILE = 'docs/LS_items.csv';
	public $ls_items;
	
	public function readItemsFile()
	{
		$this->ls_items = [];
		if (($handle = fopen(self::ITEMS_FILE, "r")) !== FALSE) {
			while (($line = fgetcsv($handle, 0, ",", '"')) !== FALSE) {
				if (count($line) != 4) {
					continue;
				}
				
				if ($line[0] == 'Item Id') {
					// header, continue to next line
					continue;
				}
				$item_id = $line[0];
				$matrix_id = $line[1];
				$manufacturer_sku = $line[2];
				$tags = $line[3];
				$this->ls_items[$manufacturer_sku] = [
					'item_id' => $item_id,
					'matrix_id' => $matrix_id,
					'tags' => $tags,
				];
			}
		}		
	}			
}

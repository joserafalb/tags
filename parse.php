<?php
/**
 * Created by PhpStorm.
 * User: jose.lopez
 * Date: 10/11/2018
 * Time: 3:30 PM
 */

$xml_string = file_get_contents('items.xml');
$xml = simplexml_load_string($xml_string);

$csv = '';

foreach ($xml->Item as $item) {

    if ($item->manufacturerSku == '') {
        continue;
    }

    $csv .= $item->itemID . '|' . $item->itemMatrixID . '|' . $item->manufacturerSku . '|';



    foreach ($item->Tags->tag as $tag) {
        $csv .= $tag . ',';
    }

    $csv .= "\r\n";
}

file_put_contents('items.csv', $csv);

echo 'fin';